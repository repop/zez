extends Control

var goback
var onPressed = [_godot, _squareKids, _peachtea, _benjobanjo]
# Called when the node enters the scene tree for the first time.
func _ready():
	DisplayServer.window_set_min_size(Vector2i(ceil(DisplayServer.screen_get_size().x * 0.2), ceil(DisplayServer.screen_get_size().y * 0.2)))
	goback = $Field/Title/goback
	for n in len($FlowContainer.get_children()):
		$FlowContainer.get_child(n).pressed.connect(onPressed[n])
	get_viewport().size_changed.connect(_resize)
	pass # Replace with function body.

func _resize():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	print(modulate.a)
	pass

func _on_goback_pressed():
	var tween = create_tween()
	tween.tween_property(self, 'modulate', Color(1, 1, 1, 0), 0.5)
	tween.finished.connect(func(): queue_free())
	pass # Replace with function body.

func _godot():
	OS.shell_open('https://godotengine.org/license/')
	pass

func _benjobanjo():
	OS.shell_open('https://opengameart.org/users/benjobanjo')
	pass

func _peachtea():
	OS.shell_open('https://opengameart.org/users/youre-perfect-studio')
	pass

func _squareKids():
	OS.shell_open('https://www.dafont.com/square-kids.font')
	pass
