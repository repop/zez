extends CharacterBody2D

@export var speed := 100
var originalSpeed := speed
var dir := Vector2.ZERO

var scalee

@export var max_tail_len = 100 

var size

var font = "res://fonts/SquareKids.ttf"

var songs := ["res://songs/8bit Bossa.mp3", "res://songs/elevator muzak - benjobanjo.ogg",
"res://songs/ElevatorMusic2.ogg", "res://songs/less_wine_and_more_crying.ogg",
"res://songs/Peachtea - Somewhere in the Elevator.ogg", "res://songs/two_left_socks.ogg",
"res://songs/which_brand_of_mustard_shall_i_buy.ogg"]

func update_size():
	size = $Sprite2D.texture.get_size()
	$Collider.shape.size = size /2
	
func _ready():
	update_size()
	scalee = max(DisplayServer.screen_get_size().x/ 640, DisplayServer.screen_get_size().y/ 640)
	position = get_viewport().position /2
	randir()
	$Sprite2D.scale *= scalee *2
	get_viewport().size_changed.connect(_resize)
	ran_becky_color()
	rand_icon()

func _resize():
	ran_becky()
	pass

func randir() -> void:
	dir = Vector2((randi() % 3) -1, (randi() % 3) -1)
	while dir.x == 0 or dir.y == 0:
		dir = Vector2((randi() % 3) -1, (randi() % 3) -1)
	pass

func ran_music() -> void:
	for music in get_tree().get_nodes_in_group('music'):
		music.stream = load(songs[randi() % len(songs)])
		music.playing = true
	pass

func ran_becky_color() -> void:
	var tween = create_tween()
	var finalColor = Color(randf_range(0.5, 1), randf_range(0.5, 1), randf_range(0.5, 1))
	for becky in get_tree().get_nodes_in_group('becky'):
		tween.tween_property(becky, 'modulate', finalColor, 0.5)
		for background in get_tree().get_nodes_in_group('background'):
			background.modulate = becky.modulate
	pass

func ran_becky() -> void:
	pass

func random_tail():
	pass
	#var finalColor = Color(randf_range(0.3, 1), randf_range(0.3, 1), randf_range(0.3, 1))
	#$Tail.gradient = finalColor

func rand_icon():
	var icons = ["res://assets/img/banana.svg", "res://assets/img/zezKids.svg", "res://assets/img/star.svg"]
	$Sprite2D.texture = load(icons[randi() % len(icons)])
	update_size()
	pass

func ran_tex() -> void:
	var beckies = ["res://assets/img/text1.png", "res://assets/img/text2.png"]
	var tween = create_tween()
	for becky in get_tree().get_nodes_in_group('becky'):
		for background in get_tree().get_nodes_in_group('background'):
			background.modulate = becky.modulate
			background.texture = load(beckies[randi() % len(beckies)])
			becky.modulate.a = 1
			tween.tween_property(becky, 'modulate', becky.modulate * Color(1, 1, 1, 0), 0.5)
			tween.finished.connect(func(): becky.texture = background.texture)
			tween.tween_property(becky, 'modulate', becky.modulate * Color(1, 1, 1, 1), 0.5)

func rand_property():
	var rans = [ran_becky_color, rand_icon]
	#for n in 2:
	rans[randi() % len(rans)].call()

func _input(event):
	for main in get_tree().get_nodes_in_group('main'):
		if get_tree().get_nodes_in_group('about') == []:
			if event is InputEventScreenDrag and event.position.y >= get_viewport().size.y * 0.9:
				print('position Y: ' + str(event.position.y))
				print('relative Y: ' + str(event.relative.y))
				if event.relative.y <= -10:
					var tween = create_tween()
					var boutinstance = load("res://src/scenes/about/about_page.tscn").instantiate()
					boutinstance.modulate.a = 0
					main.add_child(boutinstance)
					var thanks = main.get_node('Thanks')
					tween.tween_property(thanks, 'modulate', Color(1, 1, 1, 1), 0.5)
	if event is InputEventMouseButton and !event.is_pressed():
			speed += 60
			rand_property()
		#print('dir changed to :\nx: ' + str(dir.x) + '\ny: ' + str(dir.y))
		#randir()
func _physics_process(delta):
	if speed > originalSpeed:
		speed -= 5 * delta
	velocity = dir * speed
	var viewSize = get_viewport().size
	position.x = clamp(position.x, 0, viewSize.x)
	position.y = clamp(position.y, 0, viewSize.y)
	if position.x -size.x /4 <= 0:
		dir.x = 1
		#blast()
	elif position.x +size.x/4 >= viewSize.x:
		dir.x = -1
		#blast()
	if position.y -size.y /4 <= 0:
		dir.y = 1
		#blast()
	elif position.y +size.y /4 >= viewSize.y:
		dir.y = -1
		#blast()
	move_and_slide()

func _process(delta):
	var tex = $Sprite2D/TextureRect
	if tex.texture.region.position.x <= 0:
		tex.texture.region.position = Vector2(180, 180)
	tex.texture.region.position -= Vector2(velocity.x, velocity.x) * delta
	$Tail.add_point(global_position)
	if $Tail.get_point_count() > max_tail_len:
		$Tail.remove_point(0)
	pass
