extends Control

var time := 0.0
# Called when the node enters the scene tree for the first time.
func _ready():
	_change_music()
	$music.finished.connect(_change_music)
	pass # Replace with function body.

func _change_music():
	var songs = ["res://assets/songs/8bit Bossa.mp3", "res://assets/songs/elevator muzak - benjobanjo.ogg",
	"res://assets/songs/ElevatorMusic2.ogg", "res://assets/songs/less_wine_and_more_crying.ogg",
	"res://assets/songs/Peachtea - Somewhere in the Elevator.ogg", "res://assets/songs/two_left_socks.ogg",
	"res://assets/songs/which_brand_of_mustard_shall_i_buy.ogg"]
	$music.stream = load(songs[randi() % len(songs)])
	$music.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	time += 0.05
	if time >= 60:
		time = 0
	if time >= 12:
		if has_node('Hint'):
			$Hint.queue_free()
	if has_node('Hint'):
		$Hint.position.y += sin(time * 4)/2
	pass
